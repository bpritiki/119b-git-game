READ ME
MANAGER: Ben Pritikin
DEVELOPER: Alex Watson
Assignment Description:
Make a map of the Pacific ocean using CartoPy.
Use any map projection you wish; have the ocean and land colored in and have coastal boarders.
Have two labled points, one on Santa Cruz and the other can be your choice.
Hopefully that is clear enough. If not, contact me and I will elaborate.
