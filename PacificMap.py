# Map of the Pacific here :)
# if you have never used/installed cartopy on your computer you might have to>> pip install cartopy
# more info under: https://scitools.org.uk/cartopy/docs/latest/installing.html
<<<<<<< PacificMap.py
#import modules
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt

#Define Lats/Lons oflocations
Pacific_bounds = [120, 290, -65, 80]
Santa_cruz = (-122.0308, 36.9741)
Honolulu = (-157.8583, 21.3069)
Auckland = (174.7645, -36.8509)

#plot map
fig =  plt.figure()
#set up plot
ax = plt.axes(projection=ccrs.PlateCarree(central_longitude=180))
ax.set_extent(Pacific_bounds, crs =ccrs.PlateCarree())
ax.set_title("Map of Pacific Ocean")
#plot points & label them
ax.plot(Santa_cruz[0], Santa_cruz[1], markersize=5, marker='o', color='red', transform=ccrs.PlateCarree(), label="Santa Cruz")
ax.annotate('Santa Cruz', xy=(Santa_cruz[0], Santa_cruz[1]+4), xycoords=ccrs.PlateCarree()._as_mpl_transform(ax), color='black', ha='left', va='center')
ax.plot(Honolulu[0], Honolulu[1], markersize=5, marker='o', color='red', transform=ccrs.PlateCarree())
ax.annotate('Honolulu', xy=(Honolulu[0], Honolulu[1]-5), xycoords=ccrs.PlateCarree()._as_mpl_transform(ax), color='black', ha='left', va='center')
ax.plot(Auckland[0], Auckland[1], markersize=5, marker='o', color='red', transform=ccrs.PlateCarree())
ax.annotate('Auckland', xy=(Auckland[0], Auckland[1]+4), xycoords=ccrs.PlateCarree()._as_mpl_transform(ax), color='black', ha='left', va='center')
#add gridlines & map features
gl = ax.gridlines(linestyle=":", draw_labels=True)
ax.add_feature(cfeature.LAND, color='lightgray')
ax.add_feature(cfeature.OCEAN)
ax.add_feature(cfeature.COASTLINE)
